import datetime

from django import forms


class DatabaseForm(forms.Form):
    ref_date = forms.DateField(label='refdate', initial=datetime.date.today())


class UpdateForm(forms.Form):
    email = forms.EmailField()
    ref_date = forms.DateField(label='refdate', initial=datetime.date.today())
