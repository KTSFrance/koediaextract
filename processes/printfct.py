


# function print_stop_sales
#input:
# - queryset: avail_set
# - Subscriber subscriber
# Output
# - String Output
def print_stop_sales(avail_set,subscriber):
    stream=''
    avails = avail_set.filter(modifications__contains='CLOSE;').order_by('room__code', 'use_date')
    for avail in avails:
        price = avail.selling_price(subscriber)
        stream += "%s\t%s\t%d\t%s\n" % (avail.room.label_EN, avail.use_date, price, 'STOP')
    return stream

# function print_open_sales
#input:
# - queryset: avail_set
# - Subscriber subscriber
# Output
# - String Output
def print_open_sales(avail_set,subscriber):
    stream=''
    avails = avail_set.filter(modifications__contains='OPEN;')
    avails2 = avail_set.filter(modifications='NEW;')
    avails = (avails | avails2).order_by('room__code', 'use_date')
    for avail in avails:
        price = avail.selling_price(subscriber)
        stream += "%s\t%s\t%d\t%s\n" % (avail.room.label_EN, avail.use_date, price, 'OPEN')
    return stream


# function print_stop_open_sales
#input:
# - queryset: avail_set
# - Subscriber subscriber
# Output
# - String Output
def print_stop_open_sales(avail_set,subscriber):
    stop_sales=print_stop_sales(avail_set,subscriber)
    open_sales=print_open_sales(avail_set,subscriber)
    output=""
    if stop_sales!="" or open_sales!="":
        output += "STOP/OPEN SALES\n\n"
        output += 'Room name\tCheck-in\tPrice\tStop/Open\n'
        output+=stop_sales
        output+=open_sales
    return output


# function print_prices_increase
#input:
# - queryset: avail_set
# - Subscriber subscriber
# Output
# - String Output
def print_price_changes(avail_set,subscriber):
    stream=''
    avails = avail_set.filter(modifications='PRICEUP;').order_by('room__code', 'use_date')
    avails2 = avail_set.filter(modifications='PRICEDOWN;').order_by('room__code', 'use_date')

    if avails.count() > 0 or avails2.count() > 0:
        stream += "\n\nPRICE CHANGES\n\n"
        stream += 'Room name\tcheck-in\tPrice\tOld Price\tINCREASE/DECREASE\n'
        for avail in avails:
            price = avail.selling_price(subscriber)
            old_price = avail.old_selling_price(subscriber)
            stream += "%s\t%s\t%d\t%d\t%s\n" % (
                avail.room.label_EN, avail.use_date, price, old_price, 'PRICE INCREASE')
        for avail in avails2:
            price = avail.selling_price(subscriber)
            old_price = avail.old_selling_price(subscriber)
            stream += "%s\t%s\t%d\t%d\t%s\n" % (
                avail.room.label_EN, avail.use_date, price, old_price, 'PRICE DECREASE')

    return stream
