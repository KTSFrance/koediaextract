from django.contrib import admin
# Register your models here.
from .models import UpdateDatabase, SendUpdate


class UpdateDatabaseAdmin(admin.ModelAdmin):
    list_display = ['timestamp', 'success']

    class Meta:
        model = UpdateDatabase


class SendUpdateAdmin(admin.ModelAdmin):
    list_display = ['timestamp', 'success']

    class Meta:
        model = SendUpdate


admin.site.register(SendUpdate, SendUpdateAdmin)

admin.site.register(UpdateDatabase, UpdateDatabaseAdmin)
