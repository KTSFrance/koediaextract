from processes.tasks import send_updates

from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    args = '<date>'
    help = 'Send the updates to subscribed clients'

    def handle(self, *args, **options):
        try:
            send_updates()
        except Exception, e:
            raise CommandError('Could not send the updates to customers because of the following error %s' % str(e))

        self.stdout.write('Successfully sent the updates to customers')
