from processes.tasks import clean_and_update_database_process

from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    args = '<date>'
    help = 'Update the database from Koedia backend'

    def handle(self, *args, **options):
        try:
            clean_and_update_database_process()
        except Exception, e:
            raise CommandError('Could not update the database because of the following error %s' % str(e))

        self.stdout.write('Successfully updated the database')
