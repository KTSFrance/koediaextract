import datetime
from .models import UpdateDatabase, SendUpdate


def update_database_process():
    today = datetime.datetime.today()
    one_obj = UpdateDatabase()
    try:

        one_obj.update_database(today)

    except Exception, e:
        one_obj.log += '\n' + str(e) + '\n'
    print one_obj.log
    one_obj.save()
    return one_obj


def clean_and_update_database_process():
    today = datetime.datetime.today()
    one_obj = UpdateDatabase()
    try:
        one_obj.clean_and_update_database(today)
    except Exception, e:
        one_obj.log += '\n' + str(e) + '\n'
    print one_obj.log
    one_obj.save()
    return one_obj


def send_updates():
    one_obj = SendUpdate()
    try:
        one_obj.send_all_updates()
    except Exception, e:
        one_obj.log += '\n' + str(e) + '\n'
    print one_obj.log
    one_obj.save()
