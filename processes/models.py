import requests
import datetime


from django.db import models
# Create your models here.
from .printfct import print_stop_open_sales, print_price_changes
from prices.models import Program, Availability
from subscribers.models import Subscriber
from hotels.models import Room
from koediaextract.settings import EXTRACTION_URL, EMAIL_FROM, FAIL_SILENTLY, SUBJECT_EMAIL, DEBUG
from django.core.mail import send_mail
import pdb

# get_data_from_koedia_ws
# input
# model Program
# datetime refdate
# log text
# Output
# Boolean success True or False if the process succeeded or not
# String Log
# l array containing the data
def get_data_from_koedia_ws(program, ref_date):
    # we get the parameters
    params = program.request_params(ref_date)
    log = 'Sending request to Koedia:\n'
    success = False
    # we send the request
    try:
        r = requests.get(EXTRACTION_URL, params=params)
        success = True
    except:
        log += ' ---%s- could not connect to Koedia Server\n' % datetime.datetime.now()
    log += 'URL: %s\n' % str(r.url)
    log += 'Request successfully sent and received\n'
    # we clean the request
    l = r.text.split('\n')
    l = l[1:]
    l = [data_line.replace('"', '').split(';') for data_line in l]
    log += 'Number of element in answer: %s\n' % str(len(l))
    return success, log, l


# get_room_and_update
# Input:
# - data is a list of string
# - model Program
# - Dictionary room_cache
# Output:
# - Object room
# - Dictionary room_cache
# - String log
def get_room_and_update(data, program, room_cache):
    code = data[0]
    log = ''
    if code not in room_cache:
        room = Room.objects.update_room(data, program.hotel)
        room_cache[code] = room
    else:
        room = room_cache[code]
    return room, room_cache, log


class UpdateDatabase(models.Model):
    log = models.TextField(default="")
    description = models.TextField(null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    success = models.BooleanField(default=False)

    def __unicode__(self):
        return "execution %s" % self.pk

    def clean_and_update_database(self, ref_date):
        Availability.objects.all().delete()
        self.update_database(ref_date)

    def update_database(self, ref_date):
        hotel_cache = {}
        room_cache = {}

        self.log += ' ---%s- starting updating database\n' % datetime.datetime.now()
        programs = Program.objects.all()

        # If checks have already been done for this date we delete them
        self.log += "-- Checking if any records with same ref_date are present"
        if Availability.objects.filter(ref_date=ref_date).count() > 0:
            self.log += '---%s- there are records with the same reference date please check database the process will exit' % datetime.datetime.now()
            return 1

        # we update the data
        for program in programs:
            saved, not_saved, bad_data = 0, 0, 0

            self.log += ' \n---%s- Analyzing program %s for hotel %s \n' % (
                datetime.datetime.now(), program.title, program.hotel.title)

            success, log, data_array = get_data_from_koedia_ws(program, ref_date)
            self.log += log
            if not success:
                continue

            try:
                for data in data_array:
                    if len(data) > 1:
                        room_code = data[0]
                        # We update the hotel min age max age ...

                        if program.hotel.pk not in hotel_cache:
                            program.hotel.update(data)
                            hotel_cache[program.hotel.pk] = program.hotel

                        # We upload and update the rooms
                        room, room_cache, log = get_room_and_update(data, program, room_cache)
                        self.log += log
                        # we upload the availabilities
                        avail = Availability()
                        success = avail.load(data, room, program)
                        if not success:
                            bad_data += 1
                            continue
                        # We check the modifications compared to previous record

                        save_record = avail.check_modifications()
                        if save_record:
                            avail.save()
                            if avail.pk is not None:
                                saved += 1
                            else:
                                not_saved += 1
            except Exception, e:
                self.log += ' ---%s- Something went wrong while analyzing koedia data exiting... \n' % datetime.datetime.now()
                self.log += str(e)
                continue
            self.log += 'Number of element saved: %s\n' % str(saved)
            self.log += 'Number of element that should have been saved but did not: %s\n' % str(not_saved)
            self.log += 'Number of bad data: %s\n' % str(bad_data)
        Availability.objects.delete_old(ref_date)
        self.success = True
        return 0



class SendUpdate(models.Model):
    log = models.TextField(default="")
    description = models.TextField(default="")
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    success = models.BooleanField(default=False)

    def __unicode__(self):
        return "execution %s" % self.pk

    def send_all_updates(self):
        self.success = True
        N=0
        i=0
        try:
            subscribers = Subscriber.objects.filter(active=True)
            N=subscribers.count()
            for subscriber in subscribers:
                fail = self.send_update(subscriber)
                if fail>0:
                    self.success=False
                else:
                    i+=1
        except Exception, e:
            self.log += "Unexpected error:%s" % str(e)
            self.save()
            raise

## function create_update_summary:
# Create an update summary for one program and one subscriber
# It will return an empty value if no updates

    def create_update_summary(self, program, subscriber):
        self.log += ' -%s- create update summary for %s %s\n' % (
            datetime.datetime.now(), program.title, program.hotel.title)
        output = '------------------------------------------------------------------------------\n'
        output += "Reference Date: %s \n" % Availability.objects.first().ref_date
        try:
            output += "Previous update date: %s \n" % Availability.objects.filter(
                old_ref_date__isnull=False).first().old_ref_date
        except:
            output += 'No previous update\n'
        output += "Hotel: %s \n" % program.hotel.title
        output += "Program Name: %s \n\n" % program.title

        avail_set = Availability.objects.filter(program=program, modified=True)
        # if no modifications we exit and return an empty string
        if avail_set.count() == 0:
            output += "NO STOP/OPEN SALES\n\n"
            self.log += output
            output=""
        else:
            output+=print_stop_open_sales(avail_set,subscriber)
            output+=print_price_changes(avail_set,subscriber)
            output += '------------------------------------------------------------------------------\n'
        self.log += output
        self.log += ' -%s- success\n' % datetime.datetime.now()
        return output

    def send_update(self, subscriber):
        self.log += ' --%s- sending updates to %s\n' % (datetime.datetime.now(), subscriber.user.email)
        try:
            to_email = [subscriber.user.email]
            subject = SUBJECT_EMAIL
            programs = subscriber.get_program()
            if programs.count() == 0:
                self.log += ' -%s- no programs linked to the user\n' % datetime.datetime.now()
                self.description += "%s - not sent\n" % subscriber.user.email
            output = ''
            for program in programs:
                output += self.create_update_summary(program, subscriber)

            from_email = EMAIL_FROM
            if output!="":
                sent = send_mail(subject,
                                 output,
                                 from_email,
                                 to_email,
                                 # html_message=some_html_message,
                                 fail_silently=FAIL_SILENTLY)
                # sent = send_mail(subject,
                #                  output,
                #                  from_email,
                #                  ('marc.fiani@ktstravel.com',),
                #                  # html_message=some_html_message,
                #                  fail_silently=FAIL_SILENTLY)
            if sent == 0:
                self.log += ' --%s- failed to send updates to %s\n' % (datetime.datetime.now(), subscriber.user.email)
                self.description += "%s - not sent\n" % subscriber.user.email
            else:
                self.log += ' --%s- succeeded to send updates to %s\n' % (
                    datetime.datetime.now(), subscriber.user.email)
                self.description += "%s - sent\n" % subscriber.user.email

        except Exception, e:
            self.log += ' --%s- failed to send updates to %s\n' % (datetime.datetime.now(), subscriber.user.email)
            self.log += str(e)
            self.description += "%s - not sent\n" % subscriber.user.email
            return 1
        return 0
