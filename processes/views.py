from django.shortcuts import render


# Create your views here.
def database(request):
    form = DatabaseForm()
    update_form = UpdateForm()
    if request.method == 'POST':
        if u"update_database" in request.POST:
            form = DatabaseForm(request.POST)
            if form.is_valid():
                ref_date = form.cleaned_data.get("ref_date")
                update_database(ref_date)

        if u"send_updates" in request.POST:
            update_form = UpdateForm(request.POST)

            if update_form.is_valid():
                # program=Program.objects.get(pk=1)
                # ref_date = update_form.cleaned_data.get("ref_date")
                # print create_update_summary(ref_date,program)

                subscriber = Subscriber.objects.get(pk=1)
                send_update(subscriber)

            # update_database(datetime.datetime.today())
    context = {"form": form, 'update_form': update_form}
    return render(request, "database.html", context)
