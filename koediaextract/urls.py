from hotels.views import RoomListView, HotelListView
from prices.views import ProgramListView, ProgramDetailView

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.decorators import login_required

urlpatterns = [
    # Examples:
    url(r'^$', 'prices.views.home', name='home'),
    url(r'^database/$', 'processes.views.database', name='database'),
    url(r'^contact/$', 'newsletter.views.contact', name='contact'),
    url(r'^about/$', 'koediaextract.views.about', name='about'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^programs/$', login_required(ProgramListView.as_view()), name='programs'),
    url(r'^programs/(?P<pk>\d+)/$', login_required(ProgramDetailView.as_view()), name='program_detail'),
    url(r'^rooms/$', login_required(RoomListView.as_view()), name='rooms'),
    url(r'^hotels/$', login_required(HotelListView.as_view()), name='hotels'),
    url(r'^extractions/(?P<pk>\d+)/$', 'extractions.views.extraction', name='extraction'),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
