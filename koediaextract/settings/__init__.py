from .base import *
#from .local_celery import *
from .parameters import *

try:
    from .local import *

    live = False
except:
    live = True

if live:
    from .production import *
