def intordefault(str,default=0):
    try:
        return int(str)
    except:
        try:
            return int(default)
        except:
            return 0

def floatordefault(str,default=0):
    try:
        return float(str)
    except:
        try:
            return float(default)
        except:
            return 0.0

if __name__=='__main__':
    print intordefault(1) #1
    print intordefault('-1') #-1
    print intordefault('')# 0
    print intordefault('','aa')# 0
    print intordefault('',-1) # -1
