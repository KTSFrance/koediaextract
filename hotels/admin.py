from django.contrib import admin
# Register your models here.

from .models import Hotel, Room
from prices.models import Program


class ProgramInline(admin.TabularInline):
    model = Program


class HotelAdmin(admin.ModelAdmin):
    model = Hotel
    list_display = ['__str__', 'kts_id', ]
    inlines = [
        ProgramInline,
    ]


admin.site.register(Hotel, HotelAdmin)
admin.site.register(Room)
