from .models import Room, Hotel
# Create your views here.
from django.views.generic.list import ListView


class RoomListView(ListView):
    model = Room


class HotelListView(ListView):
    model = Hotel
