from django.db import models
from .validators import validate_nospace
from koediaextract.functions import intordefault


# Create your models here.
class Hotel(models.Model):
    kts_id = models.CharField(validators=[validate_nospace], max_length=25, unique=True)
    title = models.CharField(max_length=120)
    description = models.TextField(blank=True, null=True)

    age_max_bebe = models.IntegerField(default=0)
    age_max_enfant = models.IntegerField(default=0)
    age_max_ado = models.IntegerField(blank=True, null=True)

    active = models.BooleanField(default=True)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return self.title

    def update(self, data):
        save = False

        age_max_bebe = intordefault(data[18], 0)
        age_max_enfant = intordefault(data[19], 0)
        age_max_ado = intordefault(data[20], 0)

        if self.age_max_bebe != age_max_bebe:
            self.age_max_bebe = age_max_bebe
            save = True
        if self.age_max_enfant != age_max_enfant:
            self.age_max_enfant = age_max_enfant
            save = True
        if age_max_ado > 0:
            if self.age_max_ado != age_max_ado:
                self.age_max_ado = age_max_ado
                save = True
        if save:
            self.save()

        return save


class RoomManager(models.Manager):
    def update_room(self, data, hotel):
        code = data[0]
        rooms = self.filter(hotel=hotel, code=code)
        if rooms.count() == 0:
            # We create a new room and save it
            room = Room(hotel=hotel)
            room.update(data)
            room.save()
        else:
            # We check if the room need to be updated
            room = rooms[0]
            need_update = room.need_update(data)
            if need_update:
                room.update(data)
                room.save()

        return room


class Room(models.Model):
    hotel = models.ForeignKey(Hotel)
    code = models.CharField(max_length=25)
    description = models.TextField(blank=True, null=True)
    # label
    label_EN = models.CharField(max_length=120)
    label_FR = models.CharField(max_length=120, blank=True, null=True)
    label_AL = models.CharField(max_length=120, blank=True, null=True)
    label_ES = models.CharField(max_length=120, blank=True, null=True)
    label_IT = models.CharField(max_length=120, blank=True, null=True)
    label_RS = models.CharField(max_length=120, blank=True, null=True)

    min_pers = models.IntegerField(default=0)
    max_pers = models.IntegerField(default=0)

    min_adult = models.IntegerField(default=0)
    max_adult = models.IntegerField(default=0)

    min_bebe = models.IntegerField(default=0)
    max_bebe = models.IntegerField(default=0)

    min_enfant = models.IntegerField(default=0)
    max_enfant = models.IntegerField(default=0)

    min_ado = models.IntegerField(blank=True, null=True)
    max_ado = models.IntegerField(blank=True, null=True)

    active = models.BooleanField(default=True)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    objects = RoomManager()

    def __unicode__(self):
        return "%s of %s" % (self.label_EN, self.hotel.title)

    def need_update(self, data):
        update = False
        if (self.label_EN != data[1]):
            update = True

        # TODO: Completer et checker adult bebe enfant

        return update

    def update(self, data):
        self.code = data[0]
        self.label_EN = data[1]
        self.label_FR = data[2]
        self.label_AL = data[3]
        self.label_ES = data[4]
        self.label_IT = data[5]
        self.label_RS = data[6]
        # Date=data[7]
        # TODO modify here
        self.max_pers = intordefault(data[8])
        self.min_pers = intordefault(data[9])
        self.max_adult = intordefault(data[10])
        self.min_adult = intordefault(data[11])
        self.max_enfant = intordefault(data[12])
        self.min_enfant = intordefault(data[13])

        self.max_ado = intordefault(data[14])
        self.min_ado = intordefault(data[15])
        self.max_bebe = intordefault(data[16])
        self.min_bebe = intordefault(data[17])
        pass
