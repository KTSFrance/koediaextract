# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import hotels.validators

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('hotels', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hotel',
            name='kts_id',
            field=models.CharField(unique=True, max_length=25, validators=[hotels.validators.validate_nospace]),
        ),
    ]
