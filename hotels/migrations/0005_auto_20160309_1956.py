# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hotels', '0004_room_description'),
    ]

    operations = [
        migrations.AlterField(
            model_name='room',
            name='max_adult',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='room',
            name='max_bebe',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='room',
            name='max_enfant',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='room',
            name='max_pers',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='room',
            name='min_adult',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='room',
            name='min_bebe',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='room',
            name='min_enfant',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='room',
            name='min_pers',
            field=models.IntegerField(default=0),
        ),
    ]
