# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Hotel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('kts_id', models.CharField(unique=True, max_length=25)),
                ('title', models.CharField(max_length=120)),
                ('description', models.TextField(null=True, blank=True)),
                ('age_max_bebe', models.IntegerField()),
                ('age_max_enfant', models.IntegerField()),
                ('age_max_ado', models.IntegerField(null=True, blank=True)),
                ('active', models.BooleanField(default=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=25)),
                ('label_EN', models.CharField(max_length=120)),
                ('label_FR', models.CharField(max_length=120, null=True, blank=True)),
                ('label_AL', models.CharField(max_length=120, null=True, blank=True)),
                ('label_ES', models.CharField(max_length=120, null=True, blank=True)),
                ('label_IT', models.CharField(max_length=120, null=True, blank=True)),
                ('label_RS', models.CharField(max_length=120, null=True, blank=True)),
                ('min_pers', models.IntegerField()),
                ('max_pers', models.IntegerField()),
                ('min_adult', models.IntegerField()),
                ('max_adult', models.IntegerField()),
                ('min_bebe', models.IntegerField()),
                ('max_bebe', models.IntegerField()),
                ('min_enfant', models.IntegerField()),
                ('max_enfant', models.IntegerField()),
                ('min_ado', models.IntegerField(null=True, blank=True)),
                ('max_ado', models.IntegerField(null=True, blank=True)),
                ('active', models.BooleanField(default=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('hotel', models.ForeignKey(to='hotels.Hotel')),
            ],
        ),
    ]
