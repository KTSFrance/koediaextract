# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('hotels', '0002_auto_20151012_2031'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hotel',
            name='age_max_bebe',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='hotel',
            name='age_max_enfant',
            field=models.IntegerField(default=0),
        ),
    ]
