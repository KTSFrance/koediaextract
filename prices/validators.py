from django.core.exceptions import ValidationError


def validate_nospace(value):
    if value != value.strip():
        raise ValidationError('%s contains whitespaces' % value)
