from django.contrib import admin
# Register your models here.
from .models import Program, Availability


# list_display = ['__unicode__', 'price']



class AvailabilityAdmin(admin.ModelAdmin):
    list_display = ['get_hotel', 'get_room', 'ref_date', 'use_date', 'price', 'inventory', 'modified', 'modifications']
    list_filter = ('program__hotel__title', 'ref_date','modified', 'modifications', 'room__label_EN')

    # inlines = [
    # 	ProductImageInline,
    # 	VariationInline,
    # ]

    def get_hotel(self, obj):
        return obj.program.hotel.title

    def get_room(self, obj):
        return obj.room.label_EN

    class Meta:
        model = Availability


admin.site.register(Program)

admin.site.register(Availability, AvailabilityAdmin)
