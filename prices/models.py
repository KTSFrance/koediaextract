import datetime
from math import ceil
from constance import config
from hotels.models import Hotel, Room
from koediaextract.settings import DAYS_FORWARD
from koediaextract.functions import intordefault, floatordefault
from django.core.urlresolvers import reverse
from django.db import models
from .validators import validate_nospace


# from subscribers.models import Subscriber

# Create your models here.


class Program(models.Model):
    hotel = models.ForeignKey(Hotel)
    code = models.CharField(validators=[validate_nospace], max_length=25)
    title = models.CharField(max_length=120)
    description = models.TextField(blank=True, null=True)

    active = models.BooleanField(default=True)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def request_params(self, ref_date):
        next_day = (ref_date + datetime.timedelta(days=1)).strftime('%Y-%m-%d')
        next_year = (ref_date + datetime.timedelta(days=DAYS_FORWARD)).strftime('%Y-%m-%d')
        params = {'key': 'kts', 'code': 'room_avail_rate', 'id': 73, 'p.hotel': self.hotel.kts_id,
                  'p.program': self.code, 'p.from': next_day, 'p.to': next_year}
        return params

    class Meta:
        unique_together = ('hotel', 'code')

    def get_absolute_url(self):
        return reverse('program_detail', kwargs={'pk': self.pk})

    def get_extraction_url(self):
        return reverse('extraction', kwargs={'pk': self.pk})

    def __unicode__(self):
        return "%s -- %s" % (self.title, self.hotel.title)


class AvailabilityManager(models.Manager):
    def get_previous_record(self, avail):

        past_records = self.filter(room=avail.room, program=avail.program, use_date=avail.use_date,
                                   ref_date__lt=avail.ref_date)
        if past_records.count() > 0:
            past_records = past_records.latest('ref_date')
            return past_records
        else:
            return None

    def delete_old(self, reference_date):
        records = self.filter(ref_date__lt=reference_date)
        records.delete()
        pass


class Availability(models.Model):
    room = models.ForeignKey(Room)
    program = models.ForeignKey(Program)
    ref_date = models.DateField()
    old_ref_date = models.DateField(null=True)
    use_date = models.DateField()
    price = models.DecimalField(decimal_places=2, max_digits=8)
    old_price = models.DecimalField(decimal_places=2, max_digits=8, null=True)
    currency = models.CharField(max_length=3)
    inventory = models.IntegerField()
    old_inventory = models.IntegerField(null=True)
    breakfast = models.BooleanField(default=False)
    breakfast_price = models.DecimalField(decimal_places=2, max_digits=6, null=True)
    breakfast_currency = models.CharField(max_length=3, null=True)

    modified = models.BooleanField(default=False)
    modifications = models.CharField(max_length=120, null=True)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    objects = AvailabilityManager()

    class Meta:
        unique_together = ('room', 'program', 'ref_date', 'use_date')

    def __unicode__(self):
        return "%s / %s / %s / %s " % (
            self.room.label_EN, self.room.hotel.title, str(self.use_date), str(self.ref_date))

    def selling_price(self, subs):
        return ceil(self.price * subs.mark_up_mult)

    def old_selling_price(self, subs):
        return ceil(self.old_price * subs.mark_up_mult)

    # Load data from a data line
    # Input:
    # Output:
    # success True if the data are good to be loaded False overwise
    def load(self, data, room, program):
        success=True
        self.room = room
        self.program = program
        self.ref_date = datetime.date.today()
        self.use_date = datetime.datetime.strptime(data[7], "%Y-%m-%d").date()
        try:
            self.price = float(data[21])
        except:
            success=False


        self.currency = data[22]

        if data[23] == "":
            self.inventory = config.DEFAULT_ALLOT
        else:
            self.inventory = intordefault(data[23], -1)

        try:
            self.breakfast_price = float(data[24])
            self.breakfast_currency = data[25]
            self.breakfast = True
        except:
            self.breakfast = False
        return success

    def check_modifications(self):

        # modification fields
        # CHOICELIST=(
        # 	('OPEN','Sales have opened'),
        # 	('STOP','Sales have stoped'),
        # 	('PRICEUP','Price have increased'),
        # 	('PRICEDOWN','Price have decreased'),
        # 	)

        past_record = Availability.objects.get_previous_record(self)
        if past_record is None:
            self.modified = True
            if self.inventory == 0:
                self.modifications = 'NEW;NOINV;'
            else:
                self.modifications = 'NEW;'
            if self.price > 0 or self.inventory > 0:
                return True
            else:
                return False
        else:
            self.old_inventory = past_record.inventory
            self.old_price = past_record.price
            self.old_ref_date = past_record.ref_date

        modifs = ''

        # CLOSING AVAILABILITIES
        if past_record.inventory > 0 and self.inventory <= 0:
            # Close sales
            self.modified = True
            self.modifications = 'CLOSE;'
            return True
        # PRICE HAS INCREASE
        if past_record.price < self.price:
            self.modified = True
            modifs += 'PRICEUP;'
        # PRICE HAS DECREASE
        if past_record.price > self.price:
            self.modified = True
            modifs += 'PRICEDOWN;'
        # OPENING AVALABILITIES
        if past_record.inventory <= 0 and self.inventory > 0:
            # Open sales
            self.modified = True
            modifs += 'OPEN;'

        if self.modified:
            self.modifications = modifs
            if self.inventory <= 0:
                self.modifications += 'NOINV;'

        return True
