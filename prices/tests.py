from django.test import TestCase
# Create your tests here.
from .models import Program
from .tasks import update_database
from hotels.models import Hotel


class ProgramTestCase(TestCase):
    def setUp(self):
        h1 = Hotel.objects.create(kts_id="IHTLFRMPYLDE65258_KO", title="Hotel de la Grotte", age_max_bebe=0,
                                  age_max_enfant=0)
        h2 = Hotel.objects.create(kts_id="IHTLFRIDFPAR08C02_KO", title="Chateau Frontenac", age_max_bebe=0,
                                  age_max_enfant=0)
        p1 = Program.objects.create(hotel=h1, code="FSCONT1", title="Free Sales Contrat")
        p2 = Program.objects.create(hotel=h2, code="FSCONT1", title="Free Sales Contrat")

    def test_hotels(self):
        update_database()

        # def test_hotels(self):
        #     hotel_cache={}
        #     room_cache={}

        #     programs=Program.objects.all()

        #     for program in programs:
        #         print program

        #         r=requests.get('http://ktsvoyages.othyssia-admin.koedia.com/export.htm',
        #             params=program.request_params())

        #         l=r.text.split('\n')
        #         for data_line in l[1:]:
        #             data=data_line.replace('"','').split(';')
        #             if len(data)>1:
        #                 room_code = data[0]
        #                 # We update the hotel min age max age ...
        #                 if program.hotel.pk not in hotel_cache:
        #                     program.hotel.update(data)
        #                     hotel_cache[program.hotel.pk]=program.hotel

        #                 # We upload and update the rooms
        #                 code=data[0]
        #                 if code not in room_cache:
        #                     room=Room.objects.update_room(data,program)
        #                     room_cache[code]=room
        #                 else:
        #                     room=room_cache[code]
        #                 # we upload the availabilities
        #                 avail=Availability()
        #                 avail.load_and_save(data,room)

        #     p1=Program.objects.get(pk=1)
        #     p2=Program.objects.get(pk=2)
        #     print Room.objects.filter(program=p1).count()
        #     print Room.objects.filter(program=p2).count()
        #     self.assertEqual(programs.count(), 2)
        #     self.assertEqual(len(hotel_cache), 2)
        #     self.assertTrue(Room.objects.count()>0)
