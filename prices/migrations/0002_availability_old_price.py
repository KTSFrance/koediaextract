# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('prices', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='availability',
            name='old_price',
            field=models.DecimalField(null=True, max_digits=8, decimal_places=2),
        ),
    ]
