# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('prices', '0004_extraction'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='extraction',
            name='program',
        ),
        migrations.DeleteModel(
            name='Extraction',
        ),
    ]
