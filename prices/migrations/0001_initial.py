# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('hotels', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Availability',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ref_date', models.DateField()),
                ('use_date', models.DateField()),
                ('price', models.DecimalField(max_digits=8, decimal_places=2)),
                ('currency', models.CharField(max_length=3)),
                ('inventory', models.IntegerField()),
                ('breakfast', models.BooleanField(default=False)),
                ('breakfast_price', models.DecimalField(null=True, max_digits=6, decimal_places=2)),
                ('breakfast_currency', models.CharField(max_length=3, null=True)),
                ('modified', models.BooleanField(default=False)),
                ('modifications', models.CharField(max_length=120, null=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('room', models.ForeignKey(to='hotels.Room')),
            ],
        ),
        migrations.CreateModel(
            name='Program',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=25)),
                ('title', models.CharField(max_length=120)),
                ('description', models.TextField(null=True, blank=True)),
                ('active', models.BooleanField(default=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('hotel', models.ForeignKey(to='hotels.Hotel')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='program',
            unique_together=set([('hotel', 'code')]),
        ),
    ]
