# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('prices', '0008_auto_20151012_2045'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='availability',
            unique_together=set([('room', 'ref_date', 'use_date')]),
        ),
    ]
