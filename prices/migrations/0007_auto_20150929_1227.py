# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('prices', '0006_availability_old_inventory'),
    ]

    operations = [
        migrations.AddField(
            model_name='availability',
            name='old_ref_date',
            field=models.DateField(null=True),
        ),
        migrations.AlterField(
            model_name='availability',
            name='old_inventory',
            field=models.IntegerField(null=True),
        ),
    ]
