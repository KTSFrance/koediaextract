# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('prices', '0005_auto_20150925_1045'),
    ]

    operations = [
        migrations.AddField(
            model_name='availability',
            name='old_inventory',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
