# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('prices', '0002_availability_old_price'),
    ]

    operations = [
        migrations.AddField(
            model_name='availability',
            name='program',
            field=models.ForeignKey(default=0, to='prices.Program'),
            preserve_default=False,
        ),
    ]
