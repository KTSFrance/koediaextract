# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import prices.validators

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('prices', '0007_auto_20150929_1227'),
    ]

    operations = [
        migrations.AlterField(
            model_name='program',
            name='code',
            field=models.CharField(max_length=25, validators=[prices.validators.validate_nospace]),
        ),
    ]
