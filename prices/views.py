from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from .models import Program


# Create your views here.

class ProgramListView(ListView):
    model = Program

    def get_queryset(self):
        user = self.request.user
        return Program.objects.filter(subscriber__user=user)


class ProgramDetailView(DetailView):
    model = Program


def home(request):
    title = 'Sign Up Now'
    context = {"title": title}
    return render(request, "home.html", context)
