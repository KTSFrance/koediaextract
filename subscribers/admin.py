from django.contrib import admin
# Register your models here.
from .models import Subscriber


class SubscriberAdmin(admin.ModelAdmin):
    list_display = ['__str__','active']

admin.site.register(Subscriber,SubscriberAdmin)
