# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('subscribers', '0005_subscriber_mark_up'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscriber',
            name='mark_up',
            field=models.PositiveIntegerField(default=12),
        ),
    ]
