# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('subscribers', '0003_auto_20150924_1145'),
    ]

    operations = [
        migrations.RenameField(
            model_name='subscriber',
            old_name='program',
            new_name='programs',
        ),
    ]
