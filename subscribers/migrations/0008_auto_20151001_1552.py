# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('subscribers', '0007_auto_20151001_1549'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscriber',
            name='mark_up',
            field=models.DecimalField(default=0.12, max_digits=4, decimal_places=2),
        ),
    ]
