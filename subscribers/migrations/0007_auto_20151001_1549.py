# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('subscribers', '0006_auto_20151001_1519'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscriber',
            name='mark_up',
            field=models.DecimalField(default=0.12, max_digits=3, decimal_places=2),
        ),
    ]
