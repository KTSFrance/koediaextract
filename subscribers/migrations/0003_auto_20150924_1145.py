# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('prices', '0004_extraction'),
        ('subscribers', '0002_auto_20150924_1138'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='subscriber',
            name='program',
        ),
        migrations.AddField(
            model_name='subscriber',
            name='program',
            field=models.ManyToManyField(to='prices.Program'),
        ),
    ]
