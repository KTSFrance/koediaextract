from __future__ import division

from prices.models import Program

from django.conf import settings
from django.db import models


# Create your models here.


class Subscriber(models.Model):
    programs = models.ManyToManyField(Program)
    title = models.CharField(max_length=120)
    remarks = models.TextField(blank=True)
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    mark_up = models.DecimalField(default=0.12, decimal_places=2, max_digits=3)
    active = models.BooleanField(default=True)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    @property
    def mark_up_mult(self):
        return self.mark_up + 1

    def __unicode__(self):
        return self.title

    def get_program(self):
        return self.programs.filter(active=True)
