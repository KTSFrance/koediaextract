import datetime

from prices.models import Program

from .models import Extraction


def export_xls(request, pk):
    response = HttpResponse(mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=mymodel.xlsx'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet("MyModel")

    row_num = 0

    columns = [
        (u"ID", 2000),
        (u"Title", 6000),
        (u"Description", 8000),
    ]

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    for col_num in xrange(len(columns)):
        ws.write(row_num, col_num, columns[col_num][0], font_style)
        # set column width
        ws.col(col_num).width = columns[col_num][1]

    font_style = xlwt.XFStyle()
    font_style.alignment.wrap = 1

    for obj in queryset:
        row_num += 1
        row = [
            obj.pk,
            obj.title,
            obj.description,
        ]
        for col_num in xrange(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response


user = request.user
try:
    subs = Subscriber.objects.get(user__id=user.id)
except:
    raise Http404('User is not allowed to access this dataset')
program = Program.objects.get(pk=pk)
extraction_time = datetime.datetime.now()
filename = program.hotel.title + "_" + program.code + "_" + str(extraction_time) + ".csv"
filename = filename.replace(' ', '').replace('-', '')
extract = Extraction(program=program, filename=filename, subscriber=subs)
extract.save()
response = HttpResponse(content_type='text/csv')
response['Content-Disposition'] = 'attachment; filename="' + filename + '"'

dataset = program.availability_set.filter(inventory__gt=0).order_by('room__code', 'use_date')
writer = csv.writer(response)
# get the hotel name
hotel = program.hotel

# ecriture description de l'hotel


writer.writerow([])
if hotel.description:
    des_list = hotel.description.encode('utf-8').strip().split('\n')
    for des in des_list:
        writer.writerow([des])

    # on boucle sur les chambres de l'hotel

writer.writerow([hotel.title])
writer.writerow([])
writer.writerow([])
for room in program.hotel.room_set.filter(active=True).all():

    data = dataset.filter(room=room).order_by('use_date')
    if data.count() == 0:
        continue

    writer.writerow([])
    writer.writerow([])
    # On ecrit les infos sur la chambre
    writer.writerow([room.label_EN.encode('utf-8').strip()])

    # TITLE ARRAY UNIT CAPACITY &  YEARS OLD INCLUSIVE
    writer.writerow(['OCCUPANCY RULES-UNIT CAPACITY', '', '', '', 'PAX DEFINITIONS-YEARS OLD INCLUSIVE'])
    writer.writerow(['', 'MIN', 'MAX', '', '', 'MIN', 'MAX'])

    writer.writerow(['TOTAL_PAX', room.min_pers, room.max_pers, '', 'ADULT', room.hotel.age_max_enfant + 1, ''])
    writer.writerow(['ADULT', room.min_adult, room.max_adult, '', 'CHILD', 0, room.hotel.age_max_enfant, ])

    writer.writerow([])

    # TITLE ARRAY OCCUPANT PER SERVICE
    # writer.writerow(['','','','','NUMBER OF OCCUPANT(S) PER SERVICE'])



    # on boucle sur les dispo de la chambre
    cache_price = 0
    data_row = None

    availability = data.first()
    writer.writerow(['CHECK-IN START', 'CHECK-IN END', 'NBR_NIGHT(S)', availability.currency])
    for availability in data:  # corrige by MFN
        price = availability.selling_price(subs)
        checkin_date = availability.use_date
        checkout_date = availability.use_date
        number_nights = 1

        if price == cache_price:
            data_row[1] = checkout_date
        # variable pour concatener
        else:
            if data_row:
                writer.writerow(data_row)
                data_row = None
            cache_price = price

            data_row = [checkin_date, checkout_date, 1, price]
            # permet d'identifier la meme chambre

    if data_row:
        writer.writerow(data_row)

return response
