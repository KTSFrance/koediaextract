from django.contrib import admin
# Register your models here.
from .models import Extraction

admin.site.register(Extraction)
