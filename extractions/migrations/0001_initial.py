# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('subscribers', '0003_auto_20150924_1145'),
        ('prices', '0005_auto_20150925_1045'),
    ]

    operations = [
        migrations.CreateModel(
            name='Extraction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('filename', models.CharField(max_length=120)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('program', models.ForeignKey(to='prices.Program')),
                ('subscriber', models.ForeignKey(to='subscribers.Subscriber')),
            ],
        ),
    ]
