from prices.models import Program
from subscribers.models import Subscriber

from django.db import models


# Create your models here.

class Extraction(models.Model):
    program = models.ForeignKey(Program)
    filename = models.CharField(max_length=120)
    subscriber = models.ForeignKey(Subscriber)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)
