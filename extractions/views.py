import datetime

from prices.models import Program
from subscribers.models import Subscriber

import unicodecsv as csv
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, Http404
from django.utils.text import slugify
from .models import Extraction


@login_required
def extraction(request, pk):
    user = request.user
    try:
        subs = Subscriber.objects.get(user__id=user.id)
    except:
        raise Http404('User isnot allowed to access this dataset')
    program = Program.objects.get(pk=pk)
    extraction_time = datetime.datetime.now()
    filename = slugify(program.hotel.title + "_" + program.code) + ".csv"
    filename = filename.replace('-', '')
    extract = Extraction(program=program, filename=filename, subscriber=subs)
    extract.save()
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="' + filename + '"'

    dataset = program.availability_set.filter(inventory__gt=0).order_by('room__code', 'use_date')
    writer = csv.writer(response, encoding='utf-8')
    # get the hotel name
    hotel = program.hotel

    # ecriture description de l'hotel

    # on boucle sur les chambres de l'hotel

    writer.writerow(['Hotel Name:', hotel.title])

    if hotel.description:
        writer.writerow([])
        writer.writerow(['Description:'])
        for s in hotel.description.split('\n'):
            writer.writerow(['', s])
    writer.writerow([])
    writer.writerow([])
    writer.writerow(['PROGRAM:', program.title])
    if program.description:
        writer.writerow([])
        writer.writerow(['Description:'])
        for s in program.description.split('\n'):
            writer.writerow(['', s])
    writer.writerow([])
    writer.writerow([])
    for room in program.hotel.room_set.filter(active=True).all():

        data = dataset.filter(room=room).order_by('use_date')
        if data.count() == 0:
            continue

        writer.writerow([])
        writer.writerow([])
        # On ecrit les infos sur la chambre
        writer.writerow([room.label_EN.strip()])
        if room.description:
            writer.writerow([])
            writer.writerow(['Description:'])
            for s in room.description.split('\n'):
                writer.writerow(['', s])
        writer.writerow([])
        writer.writerow([])

        # TITLE ARRAY UNIT CAPACITY &  YEARS OLD INCLUSIVE
        writer.writerow(['OCCUPANCY RULES-UNIT CAPACITY', '', '', '', 'PAX DEFINITIONS-YEARS OLD INCLUSIVE'])
        writer.writerow(['', 'MIN', 'MAX', '', '', 'MIN', 'MAX'])

        writer.writerow(['TOTAL_PAX', room.min_pers, room.max_pers, '', 'ADULT', room.hotel.age_max_enfant + 1, ''])
        writer.writerow(['ADULT', room.min_adult, room.max_adult, '', 'CHILD', 0, room.hotel.age_max_enfant, ])

        writer.writerow([])

        # TITLE ARRAY OCCUPANT PER SERVICE
        # writer.writerow(['','','','','NUMBER OF OCCUPANT(S) PER SERVICE'])



        # on boucle sur les dispo de la chambre
        cache_price = 0
        data_row = None

        availability = data.first()
        writer.writerow(['CHECK-IN START', 'CHECK-IN END', 'NBR_NIGHT(S)', availability.currency])
        for availability in data:  # corrige by MFN
            price = availability.selling_price(subs)
            checkin_date = availability.use_date
            checkout_date = availability.use_date
            number_nights = 1

            if price == cache_price:
                data_row[1] = checkout_date
            # variable pour concatener
            else:
                if data_row:
                    writer.writerow(data_row)
                    data_row = None
                cache_price = price

                data_row = [checkin_date, checkout_date, 1, price]
                # permet d'identifier la meme chambre

        if data_row:
            writer.writerow(data_row)

    return response
